:author: AUTHOR_NAME
:email: <AUTHOR_EMAIL>
:revision: 202x.xx.0
:icons: font
:main-title: MAIN_TITLE
:sub-title: SUB_TITLE
:description: DESCRIPTION
:git_service: GIT_SERVICE
:project_name: GIT_REPO_NAME
:project_license: Unlicense
:experimental:
:hardbreaks:


= {main-title}: {sub-title}

== What is the Project

{description}


== Status

Version {revision}

* link:./CHANGELOG[History]
* link:{git_service}/{project_name}/-/boards[Issues Board] / link:{git_service}/{project_name}/-/issues[Issues List]


== Building

. Clone a copy of the repository:
+
[subs="attributes"]
----
$ git clone https://{git_service}/{project_name}.git
----
+

. Change to the project directory:
+
[subs="attributes"]
----
$ cd {project_name}
----
+


== Usage

Please describe how to use the project.


== License

* LICENSE: link:./LICENSE[{project_license}]


== Contributors

* {author} {email}
