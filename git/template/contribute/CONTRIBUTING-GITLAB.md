# Contributing to this Project

Contributions are always welcome. Thank you for your contribution.

1.  You can may your contributions through **[merge requests]** made to this [GitLab]
repository.

2.  You can post your text changes directly on a new issue on the [issue tracker] or in a [snippet].

For those interested in making changes through the **[merge requests]**, the workflow is as follows:

1.  Register with [WiSoft GitLab Access] to create an account and select a user name.

2.  [Fork Project] into your user's namespace on GitLab.

3.  Clone the repository:

    ```bash
    $ git clone git@wisoft.hanbat.ac.kr:hsjeon/study-spring-in-action.git spring-in-action
    $ cd spring-in-action
    ```

    The main repository will be configured as your `origin` remote.

4.  Add your fork as a the `gitlab` remote.

    ```bash
    $ git remote add gitlab git@wisoft.hanbat.ac.kr:username/study-spring-in-action.git
    ```

5.  Register Issues.

6.  Edit files and create commits (repeat as needed):

    ```bash
    $ git checkout -b specific-issue origin/master
    $ edit file1 file2 file3
    $ git add file1 file2 file3
    $ git commit
    ```

7.  Push commits in your issue branch to your fork in GitLab:

    ```bash
    $ git push gitlab HEAD
    ```

7.  Visit your fork in GitLab, browse to the "**Merge Requests**" link on the left, and use the "**New Merge Request**" button in the upper right to create a Merge Request.

[WiSoft GitLab Access]: http://wisoft.hanbat.ac.kr/gitlab
[GitLab]: http://wisoft.hanbat.ac.kr/gitlab/hsjeon/study-spring-in-action
[Fork Project]: http://wisoft.hanbat.ac.kr/gitlab/hsjeon/study-spring-in-action/forks/new
[issue tracker]: http://wisoft.hanbat.ac.kr/gitlab/hsjeon/study-spring-in-action/issues
[merge requests]: http://wisoft.hanbat.ac.kr/gitlab/hsjeon/study-spring-in-action/merge_requests
[snippet]: http://wisoft.hanbat.ac.kr/gitlab/hsjeon/study-spring-in-action/snippets
