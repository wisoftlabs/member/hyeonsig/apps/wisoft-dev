#!/bin/bash

clear
set -e

APP_HOME="${APP_HOME:-$HOME/.config/wisoft/development-preferences}"

setupGitProjectInitializer() {
  local DEFAULT_ANSWER="yes"
  local ANSWER=""

  local DEFAULT_AUTHOR_NAME="WiSoft Lab."
  local AUTHOR_NAME=""

  local DEFAULT_AUTHOR_EMAIL="contact@wisoft.io"
  local AUTHOR_EMAIL=""

  local DEFAULT_PROJECT_NAME="main title: sub title"
  local PROJECT_NAME=""

  local DEFAULT_PROJECT_DESCRIPTION="Please describe your project."
  local PROJECT_DESCRIPTION=""

  local PROJECT_INIT_VERSION="$(date +%Y.%U).0"

  printf "\n${GREEN}Do you want to setup the git repository? ${NC}"
  read -e -p "(yes) " ANSWER
  echo ""

  ANSWER="${ANSWER:-${DEFAULT_ANSWER}}"
  if [ "${ANSWER}" == "yes" ] ; then

    while [ "${AUTHOR_NAME}" == "" ]
    do
      printf "${GREEN}Author name ${NC}"
      read -e -p "(default: ${DEFAULT_AUTHOR_NAME}): " AUTHOR_NAME
      AUTHOR_NAME="${AUTHOR_NAME:-${DEFAULT_AUTHOR_NAME}}"

      printf "${GREEN}Author email ${NC}"
      read -e -p "(default: ${DEFAULT_AUTHOR_EMAIL}): " AUTHOR_EMAIL
      AUTHOR_EMAIL="${AUTHOR_EMAIL:-${DEFAULT_AUTHOR_EMAIL}}"
    done

    echo ""

    while [ "${PROJECT_NAME}" == "" ]
    do
      printf "${GREEN}Project name ${NC}"
      read -e -p "(default: ${DEFAULT_PROJECT_NAME}): " PROJECT_NAME
      PROJECT_NAME="${PROJECT_NAME:-${DEFAULT_PROJECT_NAME}}"

      printf "${GREEN}Project description ${NC}"
      read -e -p "(default: ${DEFAULT_PROJECT_DESCRIPTION}): " PROJECT_DESCRIPTION
      PROJECT_DESCRIPTION="${PROJECT_DESCRIPTION:-${DEFAULT_PROJECT_DESCRIPTION}}"
    done

    echo ""
    echo -e "${BLUE}Setting up git repository.${NC}"

    /usr/bin/git init

    cp -fr $APP_HOME/git/template/.editorconfig ./
    cp -fr $APP_HOME/git/template/CHANGELOG ./CHANGELOG
    cp -fr $APP_HOME/git/template/LICENSE ./LICENSE
    cp -fr $APP_HOME/git/template/README.adoc ./

    sed -i "" -e "s/AUTHOR_NAME/${AUTHOR_NAME}/g" README.adoc
    sed -i "" -e "s/AUTHOR_EMAIL/${AUTHOR_EMAIL}/g" README.adoc

    local MAIN_TITLE=`echo $PROJECT_NAME | cut -d ":" -f1`
    local SUB_TITLE=`echo $PROJECT_NAME | cut -d ":" -f2 | tr -d ' '`

    sed -i "" -e "s/MAIN_TITLE/${MAIN_TITLE}/g" README.adoc
    sed -i "" -e "s/SUB_TITLE/${SUB_TITLE}/g" README.adoc
    sed -i "" -e "s/DESCRIPTION/${PROJECT_DESCRIPTION}/g" README.adoc

    sed -i "" -e "s/202x.xx.0/${PROJECT_INIT_VERSION}/g" README.adoc
    sed -i "" -e "s/202x.xx.0/${PROJECT_INIT_VERSION}/g" CHANGELOG

    mkdir -p ./.gitlab/issue_templates
    cp -fr $APP_HOME/git/template/issue/feature.md ./.gitlab/issue_templates/feature.md
    cp -fr $APP_HOME/git/template/issue/bug.md ./.gitlab/issue_templates/bug.md
    
    mkdir -p ./.gitlab/merge_request_templates
    cp -fr $APP_HOME/git/template/merge/basic.md ./.gitlab/merge_request_templates/basic.md

    chmod a+x ./.git/hooks/*

    local GIT_IGNORE_DEFAULT="linux,macos,windows,jetbrains+all,visualstudiocode,vim"

    printf "\n%s\n" "------------------------------------------------------------------------------"
    /usr/bin/git ignore list
    printf "\n%s\n" "------------------------------------------------------------------------------"

    printf "\n${GREEN}Please input git ignore list(default: ${GIT_IGNORE_DEFAULT}): ${NC}"
    read -e -p "" GIT_IGNORE
    /usr/bin/git ignore ${GIT_IGNORE_DEFAULT},${GIT_IGNORE} > .gitignore

    local GIT_REMOTE_DEFAULT=""
    printf "\n${GREEN}Please input your remote git remote repository address: ${NC}"
    read -e -p "" GIT_REMOTE
    echo ""

    local GIT_REMOTE="${GIT_REMOTE:-${GIT_REMOTE_DEFAULT}}"
    local GIT_SERVICE=""

    if [ "${GIT_REMOTE}" != "" ] ; then
        GIT_REPO_NAME=`basename -s .git ${GIT_REMOTE}`
    else 
        GIT_REPO_NAME=""
    fi

    case $GIT_REMOTE in
      h*)
        GIT_SERVICE=`dirname ${GIT_REMOTE#*//}`
        ;;
      g*)
        GIT_SERVICE=`dirname ${GIT_REMOTE#*@} | tr ':' '/'`
        ;;
    esac

    GIT_SERVICE="https://${GIT_SERVICE}"

    sed -i "" -e "s/GIT_SERVICE/${GIT_SERVICE//\//\\/}/g" README.adoc
    sed -i "" -e "s/GIT_REPO_NAME/${GIT_REPO_NAME}/g" README.adoc

    if [ "${GIT_REMOTE}" != "" ] ; then
      /usr/bin/git remote add origin ${GIT_REMOTE}
    fi

    printf "\n${GREEN}Initialize repository."
    /usr/bin/git commit --allow-empty -m "Initial repository"

    printf "Mission completed.\n\n"
  else
    printf "Skip setup the git repository.\n\n"
  fi
}
