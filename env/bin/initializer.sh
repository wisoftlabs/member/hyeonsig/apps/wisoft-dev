#!/bin/bash

clear
set -e

APP_HOME="${APP_HOME:-$HOME/.config/wisoft/development-preferences}"
ASDF_HOME="${APP_HOME:-$HOME/.asdf}"
SSH_HOME="${APP_HOME:-$HOME/.ssh}"
GPG_HOME="${APP_HOME:-$HOME/.gnupg}"

setupShellAndDevPackageManager() {
    echo ""
    echo -e "${BLUE}Setting up shell manager(oh-my-zsh).${NC}"
    sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

    echo ""
    echo -e "${BLUE}Setting up shell manager plugins.${NC}"
    /usr/bin/git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
    /usr/bin/git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting

    brew install bat
    brew install fd
    brew install fzf
    brew install lsd

    /usr/bin/git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k

    echo ""
    echo -e "${BLUE}Setting up dev package manager(asdf).${NC}"
    brew install asdf # /usr/bin/git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch v0.14.0

    asdf plugin add gradle
    asdf plugin add java
    asdf plugin add kotlin
    asdf plugin add nodejs
    asdf plugin add python

    echo ""
    echo -e "${BLUE}Setting up environment variables and etc.${NC}"
    cp -fr $APP_HOME/env/zsh/.zshrc ${HOME}/.zshrc
    cp -fr $APP_HOME/env/zsh/aliases.zsh ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/aliases.zsh
    cp -fr $APP_HOME/env/zsh/environment.zsh ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/environment.zsh
    cp -fr $APP_HOME/env/zsh/pathes.zsh ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/pathes.zsh
    cp -fr $APP_HOME/env/zsh/functions.zsh ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/functions.zsh

    # Plugins 항목 대체 (.zshrc)
    # 원하는 plugins 내용을 변수에 저장
    new_plugins="plugins=(
    git
    asdf
    brew
    docker
    docker-compose
    fzf
    history
    macos
    poetry
    ripgrep
    zsh-autosuggestions
    zsh-syntax-highlighting
    )"

    # .zshrc 파일에서 plugins=(git) 라인을 찾아 new_plugins 내용으로 대체
    sed -i.bak "s/plugins=(git)/$new_plugins/" ~/.zshrc
}

setupGitInitializer() {
    echo ""
    echo -e "${BLUE}Setting up environment variables and etc.${NC}"
    cp -fr $APP_HOME/env/git/.gitconfig ${HOME}/.gitconfig

    echo ""
    local NAME=""
    printf "\n${GREEN}Input your name: ${NC}"
    read -e -p "" NAME
    
    echo ""
    local EMAIL=""
    printf "\n${GREEN}Input your email: ${NC}"
    read -e -p "" EMAIL

    /usr/bin/git config --global user.name ${NAME}
    /usr/bin/git config --global user.email ${EMAIL}
    
    /usr/bin/git config --global commit.template ${APP_HOME}/env/git/template/message/commit
    /usr/bin/git config --global core.attributesfile ${APP_HOME}/env/git/template/info/.gitattributes
    /usr/bin/git config --global init.templatedir ${APP_HOME}/env/git/template
}

setupSecurityKeyInitializer() {
  local DEFAULT_ANSWER="yes"
  local ANSWER=""

  printf "\n${GREEN}Do you want to setup the security key? ${NC}"
  read -e -p "(yes) " ANSWER
  echo ""

  ANSWER="${ANSWER:-${DEFAULT_ANSWER}}"
  if [ "${ANSWER}" == "yes" ] ; then

    echo ""
    echo -e "${BLUE}Setting up SSH Key.${NC}"
    
    local EMAIL=""
    printf "\n${GREEN}Please input your email address: ${NC}"
    read -e -p "" EMAIL
    
    echo ""
    echo -e "${BLUE}Generate a SSH key.${NC}"
    ssh-keygen -t ed25519 -C ${EMAIL}

    echo ""
    printf "\n${GREEN}Copy the public key to the clipboard? ${NC}"
    read -e -p "(yes) " ANSWER
    
    echo ""
    ANSWER="${ANSWER:-${DEFAULT_ANSWER}}"
    if [ "${ANSWER}" == "yes" ] ; then
        cat ~/.ssh/id_ed25519.pub | pbcopy

        printf "The public key has been successfully copied. Register your SSH key with the Git service\n\n"
    fi

    echo ""
    printf "SSH key generation has completed successfully.\n\n"

    echo ""
    echo -e "${BLUE}Setting up GPG Key.${NC}"

    echo ""
    echo -e "${BLUE}Install the GPG program.${NC}"
    brew install gpg 

    echo ""
    echo -e "${BLUE}Generate a GPG key.${NC}"
    gpg --full-generate-key

    echo ""
    printf "SSH key generation has completed successfully.\n\n"

    echo ""
    echo -e "${BLUE}View the list of GPG keys.${NC}"
    gpg --list-secret-keys --keyid-format short

    echo ""
    local GPG_KEY_ID=$(gpg --list-secret-keys --keyid-format short | grep ^sec | cut -d'/' -f2 | cut -d' ' -f1)

    echo ""
    printf "\n${GREEN}Copy the public key to the clipboard? ${NC}"
    read -e -p "(yes) " ANSWER
    
    echo ""
    ANSWER="${ANSWER:-${DEFAULT_ANSWER}}"
    if [ "${ANSWER}" == "yes" ] ; then
        gpg --armor --export ${GPG_KEY_ID} | pbcopy
        printf "The public key has been successfully copied. Register your GPG key with the Git service\n\n"
    fi

    /usr/bin/git config --global commit.gpgsign true
    /usr/bin/git config --global user.signingkey ${GPG_KEY_ID}

    printf "Successfully completed.\n\n"
  else
    printf "Skip setup the security key.\n\n"
  fi
}
