# Gradle
export GRADLE_OPTS="-Dfile.encoding=UTF-8"
export GRADLE_USER_HOME="$HOME/.config/gradle"

# Java
export JAVA_OPTS="-Dfile.encoding=UTF-8"

# SBT
export SBT_OPTS="-Dsbt.ivy.home=$HOME/.cache/ivy -Dsbt.coursier.home=$HOME/.cache/coursier"
