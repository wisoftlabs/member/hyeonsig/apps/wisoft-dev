#!/bin/bash

fcd() {
  cd "$(find $@ -type d | fzf)";
}

frmrf() {
  rm -rf "$(find $@ -type d | fzf)";
}

mkcd() {
  dir="$*";
  mkdir -p "$dir" && cd "$dir";
}

mkcd-y() {
  dir="$*";
  year=`date +%Y`
  if [ -z "$dir" ]; then
    mkdir -p "$year" && "$year";
  else
    mkdir -p "$year-$dir" && "$year-$dir";
  fi
}

mkcd-w() {
  dir="$*";
  week=`date +%Y.%U`
  if [ -z "$dir" ]; then
    mkdir -p "$week" && "$week";
  else
    mkdir -p "$week-$dir" && "$week-$dir";
  fi
}

kill-by-port() {
  PID=$(lsof -ti ":$1")
  if [ ! -z "$PID" ]; then
      # echo "PORT: $1"
      # echo " PID: $PID"
      kill -9 $PID
      echo "OK!"
  else
      echo "No Process Found running Port $1"
  fi
}

dir-size() {
  dir="$*"
  du -sh "$dir"/* | sort -nr | grep '\dM.*'
}

docker-tags () {
  name=$1
  # Initial URL
  url=https://registry.hub.docker.com/v2/repositories/library/$name/tags/?page_size=100
  (
    # Keep looping until the variable URL is empty
    while [ ! -z $url ]; do
      # Every iteration of the loop prints out a single dot to show progress as it got through all the pages (this is inline dot)
      >&2 echo -n "."
      # Curl the URL and pipe the output to Python. Python will parse the JSON and print the very first line as the next URL (it will leave it blank if there are no more pages)
      # then continue to loop over the results extracting only the name; all will be stored in a variable called content
      content=$(curl -s $url | python -c 'import sys, json; data = json.load(sys.stdin); print(data.get("next", "") or ""); print("\n".join([x["name"] for x in data["results"]]))')
      # Let's get the first line of content which contains the next URL for the loop to continue
      url=$(echo "$content" | head -n 1)
      # Print the content without the first line (yes +2 is counter intuitive)
      echo "$content" | tail -n +2
    done;
    # Finally break the line of dots
    >&2 echo
  ) | cut -d '-' -f 1 | sort --version-sort | uniq;
}

asdf-latest() {
  GREEN='\033[0;32m'
  NC='\033[0m'

  ASDF_LIST=("bun 1" "dotnet-core 8" "elixir 1" "erlang 26" "gradle 8" "java temurin-21" "kotlin 1" "nodejs 20" "poetry 1" "python 3" "rust 1" "sbt 1" "scala 3.3")
  ASDF_LATEST_VERSION=()
  ASDF_CURRENT_VERSION=()

  for item in ${ASDF_LIST[@]}
  do
    IFS=' ' read -rA COMMAND <<< "$item"
    ASDF_LATEST_VERSION+=($(asdf latest ${COMMAND[1]} ${COMMAND[2]}))
    ASDF_CURRENT_VERSION+=($(asdf current ${COMMAND[1]} ${COMMAND[2]} | awk '{ print $2 }'))
  done

  echo "\nThe list of programs installed with asdf"
  echo "------------------------------------------------------------------------"
  for (( i = 1 ; i <= ${#ASDF_LIST[@]}; i++ ))
  do
    if [ $ASDF_LATEST_VERSION[i] = $ASDF_CURRENT_VERSION[i] ]
    then
      printf "%-16s using the latest version($ASDF_LATEST_VERSION[i])\n" "$ASDF_LIST[i]"
      # echo -e "$ASDF_LIST[i]    using the latest version($ASDF_LATEST_VERSION[i])"
    else
      printf "%-16s ${GREEN}upgrade${NC} $ASDF_CURRENT_VERSION[i] -> $ASDF_LATEST_VERSION[i]\n" "$ASDF_LIST[i]"
      # echo -e "$ASDF_LIST[i]    ${GREEN}upgrade${NC} $ASDF_CURRENT_VERSION[i] -> $ASDF_LATEST_VERSION[i]"
    fi
  done
  echo "------------------------------------------------------------------------"
}
