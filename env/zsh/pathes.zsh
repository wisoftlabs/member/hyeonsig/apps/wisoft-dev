export PATH="/usr/local/opt/gnu-sed/libexec/gnubin:$PATH"
export PATH="/usr/local/sbin:$PATH"

## System ----------------------------------------------------------------------
# icu4c
export PATH="/usr/local/opt/icu4c/bin:/usr/local/opt/icu4c/sbin:$PATH"
export LDFLAGS="-L/usr/local/opt/icu4c/lib"
export CPPFLAGS="-I/usr/local/opt/icu4c/include"
export PKG_CONFIG_PATH="/usr/local/opt/icu4c/lib/pkgconfig"

## readline
export LDFLAGS="-L/usr/local/opt/readline/lib"
export CPPFLAGS="-I/usr/local/opt/readline/include"
export PKG_CONFIG_PATH="/usr/local/opt/readline/lib/pkgconfig"

## zlib
export LDFLAGS="-L/usr/local/opt/zlib/lib"
export CPPFLAGS="-I/usr/local/opt/zlib/include"
export PKG_CONFIG_PATH="/usr/local/opt/zlib/lib/pkgconfig"


## User ------------------------------------------------------------------------
# ASDF
export ASDF_HOME="$HOME/.asdf"
export PATH="$ASDF_HOME/shims:$PATH"

# Dotnet Core
# source $HOME/.asdf/plugins/dotnet-core/set-dotnet-home.zsh

# Java
# source $HOME/.asdf/plugins/java/set-java-home.zsh

# JetBrains
# export JETBRAINS_CONFIG_HOME="$HOME/.config/jetbrains"
# export PATH="$JETBRAINS_CONFIG_HOME/script:$PATH"
