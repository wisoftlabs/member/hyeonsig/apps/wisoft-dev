# lsd
alias ls="lsd -l"
alias la="lsd -a"
alias ll="lsd -al"
alias lt="lsd --tree"

# python
alias pip="pip3"
alias python="python3"

# homebrew
alias brew="env PATH=${PATH//$(asdf where python)\/shims:/} brew"
alias brew-latest="omz update; asdf plugin update --all &>/dev/null && brew update && brew upgrade && brew upgrade --cask && brew cleanup && asdf-latest"
