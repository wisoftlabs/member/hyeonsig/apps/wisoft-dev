#!/bin/bash

clear
set -e

APP_HOME="${APP_HOME:-$HOME/.config/wisoft/development-preferences}"

. $APP_HOME/env/bin/initializer.sh
. $APP_HOME/git/bin/initializer.sh
# . $APP_HOME/shell/env/setup-git-environment.sh
# . $DEVPRE_HOME/shell/java/setup-java.sh
# . $DEVPRE_HOME/shell/dev/setup-java-spring.sh
# . $DEVPRE_HOME/shell/dev/setup-kotlin.sh
# . $DEVPRE_HOME/shell/dev/setup-kotlin-spring.sh
# . $DEVPRE_HOME/shell/dev/setup-typescript.sh
# . $DEVPRE_HOME/shell/typescript/setup-typescript-nest.sh
# . $DEVPRE_HOME/shell/dev/setup-typescript-nuxt.sh
# . $DEVPRE_HOME/shell/dev/setup-typescript-react-vite.sh

NC="\033[0m"
RED="\033[0;31m"
GREEN="\033[1;32m"
BLUE="\033[1;34m"
CYAN="\033[1;36m"

DEFAULT_ANSWER="yes"
ANSWER=""
COMMAND=""

env() {
  option=0
  until [ "$option" = "q" ]; do
    clear
    echo ""
    echo -e "${BLUE}Environment Initializer ${NC}"
    echo ""
    echo -e " 1. Setting up Shell & Dev. Package manager"
    echo -e " 2. Setting up Git"
    echo -e " 3. Setting up Sec. Keys"
    echo -e " q. Return to menu"
    echo ""
    printf "${GREEN}Please enter your choice: ${NC}"
    read option
    echo ""
    case $option in
      1) setupShellAndDevPackageManager ;;
      2) setupGitInitializer            ;;
      3) setupSecurityKeyInitializer    ;;
      q) main                           ; press_enter ;;
      *) echo -e "\n${RED}invalid option.${NC}" ;;
    esac
  done
}

git() {
  option=0
  until [ "$option" = "q" ]; do
    clear
    echo ""
    echo -e "${BLUE}Git Initializer ${NC}"
    echo ""
    echo -e " 1. Setting up Git initializer project"
    echo -e " q. Return to menu"
    echo ""
    printf "${GREEN}Please enter your choice: ${NC}"
    read option
    echo ""
    case $option in
      1) setupGitProjectInitializer      ;;
      q) main                            ; press_enter ;;
      *) echo -e "\n${RED}invalid option.${NC}" ;;
    esac
    #   }
  done
}

java() {
    option=0
}

kotlin() {
    option=0
}

javascript() {
    option=0
}

typescript() {
    option=0
    until [ "$option" = "q" ]; do
    clear
    echo ""
    echo -e "${BLUE}TypeScript Initializer ${NC}"
    echo ""
    echo -e " 1. Setting up Git initializer project"
    echo -e " q. Return to menu"
    echo ""
    printf "${GREEN}Please enter your choice: ${NC}"
    read option
    echo ""
    case $option in
      1) setupTypeScriptBasic            ;;
      q) main                            ; press_enter ;;
      *) echo -e "\n${RED}invalid option.${NC}" ;;
    esac
    #   }
  done
}

main() {
  option=0
  until [ "$option" = "q" ]; do
    clear
    echo ""
    echo -e "${BLUE}Project Initializer ${NC}"
    echo ""
    echo -e " gi. Git         Initializer"
    echo -e " ja. Java        Initializer [${RED}Soon${NC}]"
    echo -e " kt. Kotlin      Initializer [${RED}Soon${NC}]"
    echo -e " js. JavaScript  Initializer [${RED}Soon${NC}]"
    echo -e " ts. TypeScript  Initializer [${RED}Soon${NC}]"
    echo -e " se. System Env. Initializer [${RED}Soon${NC}]"
    echo -e " qt. Quit"
    echo ""
    printf "${GREEN}Please enter your choice: ${NC}"

    read option
    case $option in
      gi) git              ; press_enter ;;
      # ja) java           ; press_enter ;;
      # kt) kotlin         ; press_enter ;;
      # js) javascript     ; press_enter ;;
      # ts) typescript     ; press_enter ;;
      se) env              ; press_enter ;;
      qt) echo -e "\n${BLUE}Thank you. Have a nice day.${NC}"; exit ;;
      *) echo -e "\n${RED}invalid option.${NC}" ;;
    esac
  done
}

main "$@"
