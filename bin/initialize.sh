#!/bin/zsh
set -e

USER=${USER:-$(id -u -n)}
HOME="${HOME:-$(eval echo ~$USER)}"
APP_HOME="${APP_HOME:-$HOME/.config/wisoft/development-preferences}"
ZSH="${ZSH:-$HOME/.oh-my-zsh}"

# Default settings
REPO=${REPO:-wisoftlabs/member/hyeonsig/apps/wisoft-dev}
REMOTE=${REMOTE:-https://gitlab.com/${REPO}.git}
BRANCH=${BRANCH:-main}

command_exists() {
  command -v "$@" >/dev/null 2>&1
}

if [ -t 1 ]; then
  is_tty() {
    true
  }
else
  is_tty() {
    false
  }
fi

# Adapted from code and information by Anton Kochkov (@XVilka)
# Source: https://gist.github.com/XVilka/8346728
supports_truecolor() {
    case "$COLORTERM" in
    truecolor|24bit) return 0 ;;
    esac

    case "$TERM" in
    iterm           |\
    tmux-truecolor  |\
    linux-truecolor |\
    xterm-truecolor |\
    screen-truecolor) return 0 ;;
    esac

    return 1
}

fmt_link() {
    # $1: text, $2: url, $3: fallback mode
    if supports_hyperlinks; then
        printf '\033]8;;%s\033\\%s\033]8;;\033\\\n' "$2" "$1"
        return
    fi

    case "$3" in
    --text) printf '%s\n' "$1" ;;
    --url|*) fmt_underline "$2" ;;
    esac
}

fmt_underline() {
    is_tty && printf '\033[4m%s\033[24m\n' "$*" || printf '%s\n' "$*"
}

fmt_code() {
    is_tty && printf '`\033[2m%s\033[22m`\n' "$*" || printf '`%s`\n' "$*"
}

fmt_error() {
    printf '%sError: %s%s\n' "${FMT_BOLD}${FMT_RED}" "$*" "$FMT_RESET" >&2
}

setup_color() {
    # Only use colors if connected to a terminal
    if ! is_tty; then
        FMT_RAINBOW=""
        FMT_RED=""
        FMT_GREEN=""
        FMT_YELLOW=""
        FMT_BLUE=""
        FMT_BOLD=""
        FMT_RESET=""
        return
    fi

    if supports_truecolor; then
        FMT_RAINBOW="
        $(printf '\033[38;2;255;0;0m')
        $(printf '\033[38;2;255;97;0m')
        $(printf '\033[38;2;247;255;0m')
        $(printf '\033[38;2;0;255;30m')
        $(printf '\033[38;2;77;0;255m')
        $(printf '\033[38;2;168;0;255m')
        $(printf '\033[38;2;245;0;172m')
        "
    else
        FMT_RAINBOW="
        $(printf '\033[38;5;196m')
        $(printf '\033[38;5;202m')
        $(printf '\033[38;5;226m')
        $(printf '\033[38;5;082m')
        $(printf '\033[38;5;021m')
        $(printf '\033[38;5;093m')
        $(printf '\033[38;5;163m')
        "
    fi

    FMT_RED=$(printf '\033[31m')
    FMT_GREEN=$(printf '\033[32m')
    FMT_YELLOW=$(printf '\033[33m')
    FMT_BLUE=$(printf '\033[34m')
    FMT_BOLD=$(printf '\033[1m')
    FMT_RESET=$(printf '\033[0m')
}

setup_app() {
    umask g-w,o-w
    echo "${FMT_BLUE}Cloning WiSoft Development Preferences(WDP).${FMT_RESET}"

    command_exists git || {
        fmt_error "git is not installed"
        exit 1
    }

    ostype=$(uname)
    if [ -z "${ostype%CYGWIN*}" ] && git --version | grep -Eq 'msysgit|windows'; then
        fmt_error "Windows/MSYS Git is not supported on Cygwin"
        fmt_error "Make sure the Cygwin git package is installed and is first on the \$PATH"
        exit 1
    fi

    if [ -d "$APP_HOME" ]; then
        # fmt_error "$APP_HOME already exists"
        # exit 1
        rm -rf "$APP_HOME" 2>/dev/null
    fi

    # Manual clone with git config options to support git < v1.7.2
    git init --quiet "$APP_HOME" && cd "$APP_HOME" \
    && git config core.eol lf \
    && git config core.autocrlf false \
    && git config fsck.zeroPaddedFilemode ignore \
    && git config fetch.fsck.zeroPaddedFilemode ignore \
    && git config receive.fsck.zeroPaddedFilemode ignore \
    && git remote add origin "$REMOTE" \
    && git fetch --depth=1 origin \
    && git checkout -b "$BRANCH" "origin/$BRANCH" || {
        [ ! -d "$APP_HOME" ] || {
        cd -
        rm -rf "$APP_HOME" 2>/dev/null
        }
        fmt_error "git clone of wisoft-git-env repo failed"
        exit 1
    }

    # Exit installation directory
    cd -
    echo
}

setup_env() {
    echo "${FMT_BLUE}Looking for an existing wisoft-dev config...${FMT_RESET}"
    # cp -fr $APP_HOME/init/template/wisoft-dev.zsh ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/wisoft-dev.zsh

    multiline='export WISOFT_DEV="$HOME/.config/wisoft/development-preferences"
export PATH="$WISOFT_DEV/bin:$PATH"
alias wisoft-dev="wisoft-dev.sh"'
    rc_file="$HOME/.zshrc"

    if ! grep -Fxq "$multiline" "$rc_file"; then
        echo "$multiline" >> "$rc_file"
    fi
}

print_success() {
    echo "${FMT_YELLOW}....is now installed.${FMT_RESET}\nPlease run 'source ~/.zshrc'\n"
}

main() {
    printf '\n'
    setup_color
    
    if ! command_exists zsh; then
        echo "${FMT_YELLOW}Zsh is not installed.${FMT_RESET} Please install zsh first."
        exit 1
    fi

    setup_app
    setup_env
    print_success
}

main "$@"
